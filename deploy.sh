#!/bin/bash

set -x
rm -RF public
mkdir public
npm install mustache
npx mustache cv.json template.html > public/index.html
cp style.css public/
